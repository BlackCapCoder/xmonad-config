{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}
module Config.Keybinding2 where

import XMonad
import Text.Megaparsec
import Data.Void
import Control.Monad
import Data.Proxy


type Key    = (KeyMask, KeySym)
type Parser = Parsec Void [Key]

instance Stream [Key] where
  type Token  [Key]    = Key
  type Tokens [Key]    = [Key]
  tokenToChunk  Proxy  = pure
  tokensToChunk Proxy  = id
  chunkToTokens Proxy  = id
  chunkLength   Proxy  = length
  chunkEmpty    Proxy  = null
  take1_ []            = Nothing
  take1_ (t:ts)        = Just (t, ts)
  takeN_ n s
    | n <= 0           = Just ([], s)
    | null s           = Nothing
    | otherwise        = Just (splitAt n s)
  takeWhile_           = span
  showTokens           = undefined
  reachOffset          = undefined


-- Returns the expected keys given a parser
getExpected p
  = (\case Left (TrivialError _ _ x) -> x)
  <$> parseMaybe (observing p) mempty

