module Config.Keys
  -- ( keyHandler
  -- )
  where

import Graphics.X11.Xlib.Extras (none, setEventType, setKeyEvent)
import Graphics.X11.Xlib.Misc
import qualified Graphics.X11.Types as XT
import qualified XMonad.Util.ExtensibleState as E
import XMonad hiding (Window, Workspace)
import qualified XMonad as X
import XMonad.Actions.Navigation2D
import XMonad.Actions.SwapWorkspaces
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout
import qualified XMonad.Layout.Groups.Helpers as G
import qualified XMonad.StackSet as W
import Config.Keybinding
import Data.Monoid
import Control.Monad
import Data.Maybe
import Data.Foldable
import Data.Function (fix)
import qualified Data.Map as M
import Control.Applicative
import System.Exit
-- import XMonad.Layout.ToggleLayouts
import XMonad.Layout.Groups.Wmii as Wmii
import qualified XMonad.Layout.Groups as Group

import XMonad.Util.NamedScratchpad
import Config.ManageHook
import XMonad.Actions.Warp
import qualified Debug.Trace as D
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import qualified XMonad.Util.Paste as PA

import Graphics.X11.Types
import Data.Time.Clock.POSIX
import Control.Concurrent
import XMonad.Util.Timer


-- Global mappings
global :: Mapping X Key ()
global = mconcat
  [ "<XF86MonBrightnessDown>" ~> eff $ spawn "~/scripts/lightdown"
  , "<XF86MonBrightnessUp>"   ~> eff $ spawn "~/scripts/lightup"
  , "S-<XF86MonBrightnessDown>" ~> eff $ spawn "light -S 0"
  , "S-<XF86MonBrightnessUp>"   ~> eff $ spawn "light -S 100"

  , "<XF86AudioRaiseVolume>"  ~> eff $ spawn "pulseaudio-ctl up"
  , "<XF86AudioLowerVolume>"  ~> eff $ spawn "pulseaudio-ctl down"
  , "<XF86AudioMute>"         ~> eff $ spawn "pulseaudio-ctl mute"
  ]

-- Modal mappings
modes = \case
  Normal -> mconcat
    -- Change mode
    [ [(0, xK_Control_R)] ~~> sub $ modes Window
    , [(0, 65027)]        ~~> sub $ modes Workspace

    -- Macro
    , "M4-r" ~> eff $ do
        ip <- isPlaying
        when (not ip) $ do
          ir <- isRecording
          if ir then stopRec else insert $ ((desert >>) . startRec =<<) `kmap` letters
    , "M4-S-r" ~> sub $ ((desert >>) . playRec 1 =<<) `kmap` letters

    -- Move focus
    , "M4-h" ~> eff $ windowGo L True
    , "M4-j" ~> eff $ windowGo D True
    , "M4-k" ~> eff $ windowGo U True
    , "M4-l" ~> eff $ windowGo R True

    , "M4-q" ~> eff kill
    , "M4-S-q" ~> eff $ io exitSuccess
    , "M4-<Space>" ~> eff $ sendMessage NextLayout
    , "M4-m"   ~> eff . E.put $ KeyRange withAll
    , "M4-S-m" ~> eff . E.put $ KeyRange withFocused
    -- , "M4-f" ~> eff $ sendMessage ToggleLayout
    , "M4-f" ~> eff $ sendMessage $ Toggle FULL
    , "M4-v" ~> eff $ sendMessage $ Toggle MIRROR
    , "M4-<Tab>"   ~> eff $ focusUp
    , "M4-S-<Tab>" ~> eff $ focusDown
    , "M4-n" ~> eff $ G.focusGroupUp
    , "M4-p" ~> eff $ G.focusGroupDown
    , "M-b" ~> eff $ banishScreen LowerRight
    , "M4-t" ~> eff $ withFocused $ windows . W.sink

    -- Programs
    , "M4-<Return>" ~> eff $ spawn "alacritty"
    , "M4-b"        ~> eff $ spawn "chromium"
    , "M4-g"        ~> eff $ spawn "rofi -show run"

    -- Scratchpads
    , "C-<Space>" ~> eff $ namedScratchpadAction scratchpads "Terminal"
    , "M4-c"      ~> eff $ namedScratchpadAction scratchpads "Chat"
    ]
    <> mconcat
    [ mconcat
      [ "M4-" ++ show i ~> eff $ windows . W.greedyView . (!!(i-1)) =<< workspaces <$> asks config
      , "M4-S-" ++ show i ~> eff $ windows . W.shift . (!!(i-1)) =<< workspaces <$> asks config
      ] | i <- [1..9] ]

  Window -> mconcat
    [ escape
    , "d"   ~> eff kill
    -- , "m"   ~> sub $ (flip windowSwap True =<<) `kmap` dirs
    -- , "e"   ~> sub $ (sendMessage . ExpandTowards =<<) `kmap` dirs
    -- , "S-e" ~> sub $ (sendMessage . ShrinkFrom    =<<) `kmap` dirs

    , "S-o" ~> eff $ G.moveToNewGroupUp
    , "o" ~> eff $ G.moveToNewGroupDown
    , "<Space>" ~> eff $ G.splitGroup
    , "n" ~> eff $ G.focusGroupUp
    , "p" ~> eff $ G.focusGroupDown

    , "<Tab>"   ~> eff $ focusUp
    , "S-<Tab>" ~> eff $ focusDown
    , "m l" ~> eff . (>> desert) $ G.moveToGroupDown False
    , "m h" ~> eff . (>> desert) $ G.moveToGroupUp   False
    , "s l" ~> eff . (>> desert) $ G.swapGroupDown
    , "s h" ~> eff . (>> desert) $ G.swapGroupUp
    , "s j" ~> eff . (>> desert) $ swapDown >> sendMessage Group.Refocus
    , "s k" ~> eff . (>> desert) $ swapUp >> sendMessage Group.Refocus

    , "e" ~> eff $ Wmii.zoomGroupIn
    , "S-e" ~> eff $ Wmii.zoomGroupOut
    , "r" ~> eff $ Wmii.zoomGroupReset
    , "w" ~> eff $ Wmii.toggleGroupFull

    -- Group Layouts
    , "f" ~> eff $ Wmii.groupToFullLayout
    , "t" ~> eff $ Wmii.groupToTabbedLayout
    , "c" ~> eff $ Wmii.groupToVerticalLayout
    ]
    <> (flip windowGo True =<<) `kmap` dirs
    <> number (\n -> ((desert >>) . playRec n =<<) `kmap` letters)
    -- <> ws W.shift -- Send to workspace


  Workspace -> mconcat
    [ escape
    , "s" ~> sub $ ws swapWithCurrent
    ]
    <> ws W.greedyView -- Switch to workspace


-- Escape back to normal mode
-- escape = "<Escape>" ~> eff $ E.put Normal
escape = "<Escape>" ~> eff desert

-- Direction keys
dirs = mconcat
  [ "h" ~> eff $ return L
  , "j" ~> eff $ return D
  , "k" ~> eff $ return U
  , "l" ~> eff $ return R ]

letters :: Mapping X Key Key
letters = mconcat $ map (\x -> [(0,x)] ~~> eff $ return (0, x)) [xK_a .. xK_z]

numbers :: Mapping X Key Int
numbers = mconcat $ map (\i -> show i ~> eff $ return i) [0..9]

number :: (Int -> Mapping X Key ()) -> Mapping X Key ()
number f = go f 0
  where go f n = mconcat
          [ ((desert >>) $) `kmap` f n
          , (>>= insert . go f . (+) (n*10)) `kmap` numbers
          ]


-- Workspace keys
ws x = mconcat
  [ show i ~> eff $ do w <- workspaces <$> asks config
                       windows . x $ w !! (i-1)
  | i <- [1..9] ]



----------


data Mode = Normal | Window | Workspace
          deriving (Eq, Ord, Show)

type Keymap = Mapping X Key ()


instance ExtensionClass Mode where
  initialValue = Normal

instance ExtensionClass Keymap where
  initialValue = modes Normal

instance ExtensionClass [Keymap] where
  initialValue = []


-- Can be used to pass input to more than one window at a time
newtype KeyRange = KeyRange { range :: (X.Window -> X ()) -> X () }

instance ExtensionClass KeyRange where
  initialValue = KeyRange withFocused

withAll f = withWindowSet $ mapM_ f . W.index


----------

type SmallKey = (EventType, (KeyMask, KeyCode))

keyHandler :: Event -> X All
keyHandler e = do
  handle e
  handleMacro e
  return (All True)

handleMacro e = do
  (RT l t) <- E.get
  handleTimer t e $ do
    case l of
      ((d, k):xs) -> do
        handleSmallKey k
        startTimer (d) >>= E.put . RT xs
      _ -> return ()
    return Nothing

handle e@KeyEvent { ev_event_type = t, ev_state = mask, ev_keycode = code }
  | sm <- (t, (mask, code))
  = do recRec sm
       handleSmallKey sm
handle _ = return ()

handleSmallKey sm@(t, (mask, code))
  | D.trace (show t) $ t /= keyPress = passKey t mask code
  | otherwise = do
      XConf { display = dpy, theRoot = rootw } <- ask
      sym <- io $ keycodeToKeysym dpy code 0
      let key = (mask, sym)

      r <- case runMap global key of
        Just x -> return $ Just x
        Nothing -> do
          ms <- E.get :: X [Keymap]
          flip fix ms $ \r -> \case
            [] -> return $ Nothing
            (x:xs) -> case runMap x key of
              Just a -> E.put (x:xs) >> return (Just a)
              Nothing | sym < 65500 -> r xs
                      | otherwise -> return $ Just $ passKey t mask code

      case r of
        Just x -> x
        Nothing -> do
          E.put ([] :: [Keymap])
          case runMap (modes Normal) key of
            Nothing -> passKey t mask code
            Just x -> x

runMap m k = case M.lookup k (unmap m) of
  Nothing -> Nothing
  Just (Left x) -> Just x
  Just (Right x) -> Just $ insert x


passKey t mask code = do
  f <- E.get :: X KeyRange
  -- range f $ sendKey t mask code
  case code of
    -- 29 -> do range f $ sendKey t mask 43; range f $ sendKey t mask 45
    -- 30 -> do range f $ sendKey t mask 45; range f $ sendKey t mask 46
    -- 56 -> do range f $ sendKey t mask 43; range f $ sendKey t mask 44
    -- 57 -> do range f $ sendKey t mask 45; range f $ sendKey t mask 44
    _  -> range f $ sendKey t mask code


sendKey t mask code w = withDisplay $ \d -> do
  XConf { theRoot = rootw } <- ask

  io $ do
    setInputFocus d w revertToParent currentTime
    -- allocaXEvent $ \ev -> do
    --   setEventType ev focusIn
    --   sendEvent d w True 0 ev
    allocaXEvent $ \ev -> do
      setEventType ev t
      setKeyEvent ev w rootw none mask code True
      let m = if | t == keyPress -> keyPressMask
                 | t == keyRelease -> keyReleaseMask
      sendEvent d w True m ev

insert x = do
  xs <- E.get :: X [Keymap]
  E.put $ x : xs

desert = do
  xs <- E.get :: X [Keymap]
  unless (null xs) $ E.put (tail xs)


----------

type RecordEntry = (POSIXTime, SmallKey)
data RecordState = Rec
  { recReg :: M.Map Key [RecordEntry]
  , recCur :: Maybe (POSIXTime, Key)
  }

instance ExtensionClass RecordState where
  initialValue = Rec mempty Nothing

recRec :: SmallKey -> X ()
recRec sm@(_, k) = do
  s <- E.get :: X RecordState
  case recCur s of
    Just r -> do
      time <- io $ utcTimeToPOSIXSeconds <$> getCurrentTime
      E.put s { recReg = M.adjust (\l -> (time - fst r, sm) : l) (snd r) $ recReg s }
    _ -> return ()

startRec k = do
  s    <- E.get :: X RecordState
  time <- io $ utcTimeToPOSIXSeconds <$> getCurrentTime
  E.put $ s { recReg = M.insert k [] $ recReg s, recCur = Just (time, k) }
  spawn "notify-send -t 500 'Started recording'"

stopRec = do
  s <- E.get :: X RecordState
  E.put s { recCur = Nothing }
  spawn "notify-send -t 1000 'Stopped recording'"

toggleRec k = do
  s <- E.get :: X RecordState
  case recCur s of
    Nothing -> startRec k
    _ -> stopRec

isRecording = do
  s <- E.get :: X RecordState
  case recCur s of
    Nothing -> return False
    _ -> return True


data RecTimer = RT
  { evs :: [(Rational, SmallKey)]
  , tid :: TimerId
  }

instance ExtensionClass RecTimer where
  initialValue = RT [] 0


playRec n k = do
  s <- E.get :: X RecordState
  case M.lookup k $ recReg s of
    Nothing -> spawn "notify-send -t 1000 no recording"
    Just l -> playKeystrokes n l

playKeystrokes :: Int -> [(POSIXTime, SmallKey)] -> X ()
playKeystrokes n l = do
  let l'     = reverse l
  let times  = map fst l'
  let keys   = map snd l'
  let deltas = map toRational $ zipWith (subtract) times $ tail times
  let l'' = concat . replicate n $ zip (0 : deltas) keys

  startTimer 0 >>= E.put . RT l''
  return ()

isPlaying = do
  RT x _ <- E.get :: X RecTimer
  return . not $ null x

