module Config.Layout (myLayout) where

import XMonad
import XMonad.Layout.Gaps
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Spacing
import XMonad.Layout.Groups.Wmii
import XMonad.Layout.NoBorders
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances


myLayout = id
         . smartBorders
         -- . toggleLayouts Full
         . mkToggle (FULL ?? MIRROR ?? EOT)
         . gaps (zip [U,D,L,R] $ repeat 20)
         . spacing 10
         $ wmii shrinkText def ||| emptyBSP
