module Config.Vi where

import Text.Megaparsec


data Verb
  = Delete
  | Swap
  | Rotate
  | Yank
  | Minimize
  | Mirror

data Motion
  = North | South | East | West
  | First | Last  | Top  | Bottom

data Object
  = Workspace
  | Window
  | Group
  | Screen
