module Config.Keybinding where

import qualified Data.Map as M
import XMonad
import XMonad.Util.EZConfig (readKeySequence)
import Data.Maybe
import Control.Applicative
import Data.Semigroup


type Key = (KeyMask, KeySym)

newtype Mapping m k v
  = Mapping { unmap :: M.Map k (Either (m v) (Mapping m k v)) }
  deriving (Show)


kmap f (Mapping m) = Mapping $ flip fmap m $ \case
  Left x -> Left $ f x
  Right x -> Right $ kmap f x

instance Functor m => Functor (Mapping m k) where
  fmap f = kmap $ fmap f

instance Ord k => Semigroup (Mapping m k v) where
  Mapping a <> Mapping b
    = Mapping $ M.unionWith f a b
    where f x y
            | Right ma <- x
            , Right mb <- y
            = Right $ ma `mappend` mb
            | Right ma <- x = x
            | Right mb <- y = y
            | otherwise = x

instance Ord k => Monoid (Mapping m k v) where
  mappend = (<>)
  mempty = Mapping mempty

infixr 0 ~>
k ~> a = fromJust (readKeySequence def k) ~~> a

infixr 0 ~~>
[k]    ~~> a = Mapping $ M.singleton k a
(k:ks) ~~> a = Mapping . M.singleton k . Right $ ks ~~> a

eff = Left
sub = Right
