module Config.LogHook (myLogHook) where

import XMonad
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.DynamicLog

import XMonad.Actions.UpdatePointer (updatePointer)
import XMonad.Hooks.FadeWindows     ( fadeWindowsLogHook
                                    , fadeTo
                                    , isUnfocused
                                    )

myLogHook = idHook
        <+> fadeWindowsLogHook fadeHook

fadeHook = mconcat
  [ fadeTo 0
  , className =? "Chromium" --> fadeTo 0.07
  , isFullscreen            --> fadeTo 0
  ]
