module Config.ManageHook where

import XMonad
import XMonad.Hooks.ManageHelpers
import XMonad.Util.NamedScratchpad
import XMonad.StackSet (RationalRect (..))


myManageHook = idHook
           <+> rules
           <+> namedScratchpadManageHook scratchpads

rules = composeAll
  [ className =? "xwinwrap" --> doIgnore
  ]

scratchpads =
  [ NS "Terminal" "alacritty --title drop-down-terminal" (className =? "drop-down-terminal")
       $ doRectFloat $ RationalRect 0.05 0.0 0.90 0.5

  , NS "Chat" "alacritty --title profanity -e /home/blackcap/scripts/prof" (className =? "profanity")
      $ doRectFloat $ RationalRect 0.0 0.0 0.5 1.0

  -- , NS "video" "xmessage hello" (className =? "mpv")
  --     $ doRectFloat $ RationalRect 0.735 0.72 0.25 0.25
  ]
