module Main (main) where

import XMonad
import XMonad.Hooks.EwmhDesktops (ewmh)
import qualified Data.Map as M
import XMonad.Actions.Navigation2D

import Config.Keys
import Config.Layout
import Config.ManageHook
import Config.LogHook


main :: IO ()
main = do
  xmonad . id
         . ewmh
         . withNavigation2DConfig def { defaultTiledNavigation = centerNavigation }
         $ myConfig


myConfig :: XConfig _
myConfig = def
  { terminal           = "alacritty"
  , focusFollowsMouse  = True
  , modMask            = mod4Mask
  , workspaces         = show <$> [1..9]

  -- Borders
  , borderWidth        = 1
  , normalBorderColor  = "black"
  , focusedBorderColor = "#D68F00"

  , layoutHook         = myLayout
  , logHook            = myLogHook
  , handleEventHook    = keyHandler
  , keys               = mempty
  , manageHook         = myManageHook
  }
