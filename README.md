# XMonad Config

I have been using XMonad for something like 4 years now, and the code is really organic. I've added and forgot about things incrementally, there is zero logic to the keybindings so I only remember the ones I use regularly, and it is getting painful to maintain.

Therefor, I decided to start over from scratch. To roll a fresh new config and just force myself to use it until it is on-par with the old one, and then hopefully I can end up with something that makes more sense this time around.
